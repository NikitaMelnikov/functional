<?php
/**
 * @author Nikita Melnikov <n.melnikov@dengionline.com>
 * @date 17.11.14
 */

namespace Functional\Collection;

/**
 * Class Collection
 * @package Functional\Collection
 */
class Collection extends AbstractCollection
{
    /**
     * @inheritdoc
     */
    public function __construct(array $data = [])
    {
        parent::__construct(array_values($data));
    }

    /**
     * Добавление элемента в коллекцию
     * @param mixed $item
     * @return void
     */
    public function add($item)
    {
        $this->data[] = $item;
    }

    /**
     * Создание HashMap из коллекции
     * @param \Closure $f
     * @return HashMap
     */
    public function toHashMap($f)
    {
        return (new HashMap($this->data))->replaceKeys($f);
    }
} 