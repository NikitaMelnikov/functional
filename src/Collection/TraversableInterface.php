<?php
/**
 * @author Nikita Melnikov <n.melnikov@dengionline.com>
 * @date 17.11.14
 */

namespace Functional\Collection;

/**
 * Interface TraversableInterface
 * @package Collection
 */
interface TraversableInterface extends \IteratorAggregate
{
    /**
     * Применение функции к каждому элементу в коллекции
     * @param \Closure $f
     * @return static
     */
    public function map($f);

    /**
     * Применение функции к каждому элементу в коллекции
     * @param \Closure $f
     * @return void
     */
    public function each($f);

    /**
     * Фильтрация данных по пользовательской функции
     * @param \Closure $f
     * @return static
     */
    public function filter($f);
} 