<?php
/**
 * @author Nikita Melnikov <n.melnikov@dengionline.com>
 * @date 17.11.14
 */

namespace Functional\Collection;

use Functional\Exceptions\NoSuchElementException;
use Traversable;

/**
 * Class AbstractCollection
 * @package Functional\Collection
 */
abstract class AbstractCollection implements TraversableInterface, \Countable
{
    /**
     * @var array
     */
    protected $data = [];

    /**
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->data;
    }

    /**
     * Пустая ли коллекция
     * @return boolean
     */
    public function isEmpty()
    {
        return $this->count() === 0;
    }

    /**
     * Получение первого элемента в коллекции
     * @return mixed
     * @throws NoSuchElementException
     */
    public function head()
    {
        $h = reset($this->data);
        if ($h !== false) {
            return $h;
        } else {
            throw new NoSuchElementException("EmptyCollection.head");
        }
    }

    /**
     * Получение хвоста коллекции
     * @return static
     */
    public function tail()
    {
        return new static(array_slice($this->data, 1));
    }

    /**
     * Получение N-количества элементов в коллекции
     * @param integer $n
     * @return static
     */
    public function take($n)
    {
        return new static(array_slice($this->data, 0, $n));
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     */
    public function count()
    {
        return count($this->data);
    }

    /**
     * Итеративно уменьшает массив к единственному значению, используя callback-функцию
     * @see array_reduce
     * @param \Closure $f
     * @param mixed $init Изначальное значение
     * @return mixed
     */
    public function foldLeft($f, $init)
    {
        return array_reduce($this->data, $f, $init);
    }

    /**
     * Итеративно уменьшает массив к единственному значению, используя callback-функцию
     * @see array_reduce
     * @param \Closure $f
     * @return mixed
     */
    public function reduce($f)
    {
        return $this->foldLeft($f, null);
    }

    /**
     * Применение функции к каждому элементу в коллекции
     * @param \Closure $f
     * @return static
     */
    public function map($f)
    {
        return new static(array_map($f, $this->data));
    }

    /**
     * Применение функции к каждому элементу в коллекции
     * @param \Closure $f
     * @return void
     */
    public function each($f)
    {
        $d = $this->data;
        array_walk($d, $f);
    }

    /**
     * Фильтрация данных по пользовательской функции
     * @param \Closure $f
     * @return static
     */
    public function filter($f)
    {
        return new static(array_filter($this->data, $f));
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->data);
    }
} 