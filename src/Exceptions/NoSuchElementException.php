<?php
/**
 * @author Nikita Melnikov <n.melnikov@dengionline.com>
 * @date 11.11.14
 */

namespace Functional\Exceptions;

/**
 * Class NoSuchElementException
 * @package Option\Exception
 */
class NoSuchElementException extends \Exception
{
}