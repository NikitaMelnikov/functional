<?php
/**
 * @author Nikita Melnikov <n.melnikov@dengionline.com>
 * @date 11.11.14
 */

namespace Tests\Option;

use Functional\Option\Some;

/**
 * Class SomeTest
 * @package Tests\Option
 */
class SomeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Проверка получение именного того типа, который мы ожидаем
     * @return void
     */
    public function testGet()
    {
        $value = 123;
        $this->assertEquals($value, (new Some($value))->get());
    }

    /**
     * Значение Some всегда не пустое
     * @return void
     */
    public function testIsEmpty()
    {
        $this->assertFalse((new Some(123))->isEmpty());
    }
} 