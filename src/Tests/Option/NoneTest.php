<?php
/**
 * @author Nikita Melnikov <n.melnikov@dengionline.com>
 * @date 11.11.14
 */

namespace Tests\Option;

use Functional\Option\None;
use Functional\Exceptions\NoSuchElementException;

/**
 * Class NoneTest
 * @package Tests\Option
 */
class NoneTest extends \PHPUnit_Framework_TestCase
{
    /**
     * При попытке получение значения из None должно валиться исключение
     * @return void
     */
    public function testGet()
    {
        $this->setExpectedException(NoSuchElementException::class);
        (new None())->get();
    }

    /**
     * Значение None всегда пустое
     * @return void
     */
    public function testIsEmpty()
    {
        $this->assertTrue((new None())->isEmpty());
    }
} 