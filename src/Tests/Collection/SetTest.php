<?php
/**
 * @author Nikita Melnikov <n.melnikov@dengionline.com>
 * @date 17.11.14
 */

namespace Tests\Collection;

use Functional\Collection\Set;


/**
 * Class SetTest
 * @package Tests\Collection
 */
class SetTest extends \PHPUnit_Framework_TestCase
{
    public function testConstruct()
    {
        $origin = [1, 2, 3, 1];
        $this->assertCount(4, $origin);

        $s = new Set($origin);
        $this->assertCount(3, $s);
    }
} 